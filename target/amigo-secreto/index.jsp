<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Sorteo Intercambio de Regalos</title>

        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="js/bootstrap.min.js"></script>
        <script>

            $(document).ready(function () {
                // do jQuery
                loadListJugadores();
                function loadListJugadores() {

                    $.ajax({
                        type: "GET",
                        url: "/s/init/list"
                    })
                            .done(function (data) {
                                $.each(data, function (i, item) {
                                    //alert(item.nombre);

                                    if (i == 0) {
                                        console.log(i);
                                        $('#tblJugadores >tbody').html('<tr><td>' + item.nombre + '</td><td>' + item.email + '</td></tr>');
                                    } else {
                                        console.log(i);
                                        $('#tblJugadores  tr:last').after('<tr><td>' + item.nombre + '</td><td>' + item.email + '</td></tr>');
                                    }
                                });
                                //console.log(data);
                            });

                }

                $('#modalAddJugador').on('show.bs.modal', function (event) {
                    $("#lblAddOK").hide();
                    $("#lblValidation").hide();
                });


                $('#btnSortear').on('click', function () {
                    var $btn = $(this).button('loading');
                    // business logic...
                    $.ajax({
                        type: "POST",
                        url: "/s/init/sorteo"
                    }).done(function (data) {
                        //alert("Data Saved: " + msg);
                        console.log(data);

                    });
                    $btn.button('reset');
                });

                $('#btnEliminar').on('click', function () {
                    var $btnE = $(this).button('loading');
                    // business logic...
                    $.ajax({
                        type: "POST",
                        url: "/s/init/reset"
                    }).done(function (data) {
                        //alert("Data Saved: " + msg);
                        console.log(data);
                        $('#tblJugadores >tbody').html('');
                    });
                    $btnE.button('reset');
                });


                $('#btnSaveJugador').on('click', function () {
                    var $btn = $(this).button('loading');
                    // business logic...

                    var n = $("#nombre").val();
                    var e = $("#email").val();
                    console.log("0");
                    if (n !== "" && e !== "") {
                        console.log("a");
                        $("#lblValidation").hide();
                        console.log("b");
                        console.log(n);
                        console.log(e);
                        $.ajax({
                            type: "POST",
                            url: "/s/init/addJugador",
                            data: {nombre: n, email: e}
                        }).done(function (data) {
                            //alert("Data Saved: " + msg);
                            console.log(data);
                            loadListJugadores();
                            $("#nombre").val("")
                            $("#email").val("");
                            $("#lblAddOK").show();
                        });
                    } else {
                        $("#lblAddOK").hide();
                        $("#lblValidation").show();
                    }
                    $btn.button('reset');
                });


            });



        </script>
    </head>
    <body>

        <div class="container theme-showcase" role="main">

            <div class="jumbotron page-header">
                <h2>Intercambio de Regalos</h2>
                <p>Agregue jugadores para poder iniciar el sorteo.</p>
                <p>

                    <button type="button" id="btnAddJugador" class="btn btn-primary" data-toggle="modal" data-target="#modalAddJugador" >
                        Agregar Jugador
                    </button>
                    <button type="button" id="btnSortear" data-loading-text="Loading..." data-complete-text="Terminado!" class="btn btn-primary" autocomplete="off">
                        Sortear
                    </button>
                    <button type="button" id="btnEliminar" data-loading-text="Loading..." data-complete-text="Terminado!" class="btn btn-primary" autocomplete="off">
                        Limpiar Jugadores
                    </button>
                </p>
            </div>



            <div class="table-responsive">
                <table class="table table-striped" id="tblJugadores">
                    <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Email</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>

                <!---Modal-->
                <div class="modal fade" id="modalAddJugador" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                                <h4 class="modal-title" id="exampleModalLabel">Agregar Jugador</h4>
                            </div>
                            <div class="modal-body">
                                <p style="display: none;" id="lblAddOK" class="bg-success">Jugador Agregado Correctamente</p>
                                <p style="display: none;" id="lblValidation"class="bg-danger">Ingrese datos en los campos</p>
                                <form role="form">
                                    <div class="form-group">
                                        <label for="email" class="control-label">*Email:</label>
                                        <input type="text" class="form-control" id="email" />
                                    </div>
                                    <div class="form-group">
                                        <label for="nombre" class="control-label">*Nombre:</label>
                                        <input class="form-control" id="nombre" />
                                    </div>
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                <button type="button"  id="btnSaveJugador" data-loading-text="Loading..." class="btn btn-primary">Agregar</button>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </body>
</html>