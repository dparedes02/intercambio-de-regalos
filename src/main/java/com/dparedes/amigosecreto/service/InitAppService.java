/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dparedes.amigosecreto.service;

import static com.dparedes.amigosecreto.service.OfyService.ofy;
import com.dparedes.amigosecreto.user.JugadoresDto;
import com.dparedes.amigosecreto.util.MailUtil;
import com.google.gson.Gson;
import com.googlecode.objectify.cmd.Query;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

/**
 *
 * @author dgparedes
 */
@Path("/init")
public class InitAppService {
    
    Gson gson = new Gson();

    @POST
    @Path("/addJugador")
    @Produces("application/json;charset=UTF-8")
    public Response addJugador(
            @FormParam("email") String p_email,
            @FormParam("nombre") String p_nombre,
            @Context HttpServletRequest p_request) {
        
        JugadoresDto newJugador = new JugadoresDto();
        newJugador.setEmail(p_email);
        newJugador.setNombre(p_nombre);
        ofy().save().entity(newJugador).now();
        return Response.ok().entity(gson.toJson(newJugador)).build();
    }

    @POST
    @Path("/sorteo")
    @Produces("application/json;charset=UTF-8")
    public Response sendSorteo(
            @Context HttpServletRequest p_request) {

        Query<JugadoresDto> query = ofy().
                load().
                type(JugadoresDto.class);
        List<JugadoresDto> list = query.list();

        long seed = System.nanoTime();
        Collections.shuffle(list, new Random(seed));

        for (int i = 0; i < list.size(); i++) {

            JugadoresDto jugadorI = (JugadoresDto) list.get(i);
            JugadoresDto jugadorJ = new JugadoresDto();

            if (i == (list.size() - 1)) {
                jugadorJ = (JugadoresDto) list.get(0);
            } else {
                jugadorJ = (JugadoresDto) list.get(i + 1);
            }

            MailUtil.sendEmail(jugadorI.getEmail(), "Tu amigo secreto es ......" + jugadorJ.getNombre(), "Intercambio de Regalos");

        }
        return Response.ok().entity(gson.toJson("ok")).build();
    }

    @GET
    @Path("/list")
    @Produces("application/json;charset=UTF-8")
    public Response listadoJugadores(
            @Context HttpServletRequest p_request) {
        Query<JugadoresDto> query = ofy().
                load().
                type(JugadoresDto.class);
        Gson gson = new Gson();
        return Response.ok().entity(gson.toJson(query.list())).build();
    }

    @POST
    @Path("/reset")
    @Produces("application/json;charset=UTF-8")
    public Response resetSorteo(
            @Context HttpServletRequest p_request) {
        
        ofy().delete().entities(ofy().load().type(JugadoresDto.class).list());
        
        return Response.ok().entity(gson.toJson("ok")).build();
    }

}
