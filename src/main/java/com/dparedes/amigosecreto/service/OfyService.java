/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dparedes.amigosecreto.service;

import com.dparedes.amigosecreto.user.JugadoresDto;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyFactory;
import com.googlecode.objectify.ObjectifyService;

/**
 *
 * @author dparedes
 */
public class OfyService {

    static {
        factory().register(JugadoresDto.class);
        //    factory().register(UserDto.class);

    }

    public static Objectify ofy() {
        return ObjectifyService.ofy();
    }

    public static ObjectifyFactory factory() {
        return ObjectifyService.factory();
    }

}
