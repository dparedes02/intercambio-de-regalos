/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dparedes.amigosecreto.user;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import java.io.Serializable;

/**
 *
 * @author DIEGO
 */
@Entity
public class JugadoresDto implements Serializable {

    private static final long serialVersionUID = 8484716012306489290L;

    @Id
    private String email;
    private String nombre;

    public JugadoresDto() {
    }


    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    
    
    
}
