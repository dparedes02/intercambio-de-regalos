/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dparedes.amigosecreto.user;

import com.googlecode.objectify.annotation.Entity;
import java.io.Serializable;

/**
 *
 * @author DIEGO
 */
@Entity
public class UserDto implements Serializable {

    private static final long serialVersionUID = 8484716012306489290L;

    private Long idUser;
    private String loginUser;
    private String passUser;

    public UserDto() {
    }

    /**
     * @return the idUser
     */
    public Long getIdUser() {
        return idUser;
    }

    /**
     * @param idUser the idUser to set
     */
    public void setIdUser(Long idUser) {
        this.idUser = idUser;
    }

    /**
     * @return the loginUser
     */
    public String getLoginUser() {
        return loginUser;
    }

    /**
     * @param loginUser the loginUser to set
     */
    public void setLoginUser(String loginUser) {
        this.loginUser = loginUser;
    }

    /**
     * @return the passUser
     */
    public String getPassUser() {
        return passUser;
    }

    /**
     * @param passUser the passUser to set
     */
    public void setPassUser(String passUser) {
        this.passUser = passUser;
    }

}
