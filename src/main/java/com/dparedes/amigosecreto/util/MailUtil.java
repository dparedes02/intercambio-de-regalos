/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dparedes.amigosecreto.util;

import java.io.UnsupportedEncodingException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 *
 * @author DIEGO
 */
public class MailUtil {

    public static void sendEmail(String email, String body, String title) {

        Properties props = new Properties();
        Session session = Session.getDefaultInstance(props, null);

        String msgBody = body;

        try {
            Message msg = new MimeMessage(session);
            msg.setFrom(new InternetAddress("dparedes02@gmail.com", "Interambio de Regalo"));
            msg.addRecipient(Message.RecipientType.TO,
                    new InternetAddress(email));
            msg.setSubject(title);
            msg.setText(msgBody);
            Transport.send(msg);

        } catch (AddressException e) {
            Logger.getLogger(MailUtil.class.getName()).log(Level.SEVERE, null, e);
        } catch (MessagingException e) {
            Logger.getLogger(MailUtil.class.getName()).log(Level.SEVERE, null, e);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(MailUtil.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
